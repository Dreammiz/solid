using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AttackController : MonoBehaviour
{
    [Header("Attack")]
    [SerializeField]
    public WeaponData currentWeapon;
    [SerializeField] private SpriteRenderer weaponSprite;
    [SerializeField] public Transform firePoint;
    [SerializeField] private GameObject meleeHitBox;
    private bool isAttacking;
    #region Attack

    
    
    public void PerformAttack(InputAction.CallbackContext _context)
    {
        if (!_context.performed || !currentWeapon) return;

        switch (currentWeapon.AttackType)
        {
            case AttackType.MeleeAttack:
                MeleeAttack();
                break;
            case AttackType.RangeAttack:
                RangeAttack();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    

    private void MeleeAttack()
    {
        if (isAttacking) return;
        
        weaponSprite.color = currentWeapon.weaponColor;
        StartCoroutine(IEAttack());
    }

    private void RangeAttack()
    {
        var _bullet = Bullet.GetBullet(gameObject);
        _bullet.transform.position = firePoint.position;
        _bullet.Sprite.color = currentWeapon.weaponColor;
        _bullet.Fire(firePoint.right);
    }

    private IEnumerator IEAttack()
    {
        isAttacking = true;
        meleeHitBox.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        meleeHitBox.SetActive(false);
        isAttacking = false;
    }

    #endregion
}
